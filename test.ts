import * as fs from 'fs';
import * as path from 'path';
import SpectreClient = require('nodeclient-spectre');
 
const spectreUrl = 'http://localhost:3000';
const spectreClientInstance = new SpectreClient(spectreUrl);

runPoc();

async function runPoc() {
    // read screenshot1 from file
    const screenshot1Base64 = fs.readFileSync(path.join(__dirname, '/img/lessen1.png')).toString('BASE64');

    // read screenshot2 from file
    const screenshot2Base64= fs.readFileSync(path.join(__dirname, '/img/lessen2.png')).toString('BASE64');
    
    const run = await spectreClientInstance.createTestrun("P1", "Teacher");
    const result = await spectreClientInstance.submitScreenshot("Lessen", "Chrome", 1920, screenshot1Base64, run.id, '','','10');
    console.log(result.pass);

    const run2 = await spectreClientInstance.createTestrun("P1", "Teacher");
    const result2 = await spectreClientInstance.submitScreenshot("Lessen", "Chrome", 1920, screenshot2Base64, run2.id, '','','10');
    console.log(result2.pass);
}
