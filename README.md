# nodeclient-spectre-poc  
This poc contains a script that shows how to create a new project/suite in Spectre and how to upload screenshots to this project.

## Setup Spectre
Make sure you have Spectre running on localhost:3000. The easiest way for this is using their docker container. For this make sure to:
- Clone the Spectre repo:  
  `git clone https://github.com/wearefriday/spectre.git`
- Setup the database (only needs to be done once):  
  `docker-compose run --rm app bundle exec rake db:setup`
- Run the application:  
  `docker-compose up`
- When you see `WEBrick::HTTPServer#start: pid=2 port=3000`, the app will be running at http://localhost:3000

## Running the POC
Run using `npm test`. The POC will create 2 testruns and upload 2 different images. The first will pass (baseline) and the second will fail.

## Results
View the results in Spectre at http://localhost:3000

## Admin
Manage the data using the admin console at http://localhost:3000/admin